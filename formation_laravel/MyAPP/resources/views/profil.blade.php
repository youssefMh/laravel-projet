@extends('layout')
@section('title','Profil')

@section('header')
    @parent
    <h1> profile</h1>
@endsection


@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <div class="w3-container">
        <h2>Panels for Quotes</h2>
        <p>The w3-panel class can be used to display quotes.</p>

        <div class="w3-panel w3-leftbar w3-sand w3-xxlarge w3-serif">
            <p><i>"Make it as simple as possible, but not simpler."</i></p>
        </div>
    </div>
@endsection
