<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_tags', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tag-id')->unsigned();
            $table->bigInteger('message-id')->unsigned();
            $table->foreign('tag-id')->references('id')->on('tags')->onDelete('cascade');
            $table->foreign('message-id')->references('id')->on('messages')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
