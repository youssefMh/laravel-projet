<?php

use App\Http\Middleware\VerifAge;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/page1', function () {
    return '<h1>Welcome Sir Youssef</h1>';
});
Route::get('/page2/{name}', function ($name) {
    return '<h1>Welcome Sir '.$name.'</h1>';
});
Route::get('/page3/{name}/classe/{grp}', function ($name ,$grp) {
    return '<h1>Welcome Sir '.$name.'dans' .$grp.'</h1>';
});
Route::get('/page4/{name}/classe/{classe?}', function ($name ,$classe=null) {
    return '<h1>Welcome Sir '.$name.' ------' .$classe.'</h1>';
})->where('name','[A-Za-z]+' );

Route::get('/page5/{name}/classe/{classe?}', function ($name ,$classe=null) {
    return '<h1>Welcome Sir '.$name.' ------' .$classe.'</h1>';
})->where(array('name'=>'[A-Za-z]+' ,  'classe' =>'[0-9]+'));

Route::get('resp', function () {
    return response('Hello All Friends',200)->header('Content-Type','text/plain');
});

Route::get('respJson', function () {
    return response()->json(([ 'Name'=> 'Ahmed' ,'State'=>'Austria']));
});

Route::get('home', [HomeController::class, 'index'])->middleware(VerifAge::class);;
Route::get('Show_home', [HomeController::class, 'show']);
Route::get('show_profile', [HomeController::class, 'show_profile']);
Route::get('result', function (\Illuminate\Http\Request $request)
    {
        var_dump($request->Nom);
    }
);
